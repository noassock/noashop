export function formatMoney(value) {
    return new Intl.NumberFormat("fr-FR", {
        style: "currency",
        currency: "EUR",
    }).format(value);
}

export function formatVoucher(voucher) {
    return voucher * 100 + "%";
}
