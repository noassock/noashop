export function getProductDatabase() {
    return JSON.parse(localStorage.getItem('productDatabase'));
}

export function findProduct(product_code) {
    
    const productDatabase = getProductDatabase();

    return productDatabase.find((product) => product.product_code === product_code) ?? false;
}


export function findVoucher(voucherCode) {

    const voucherDatabase = JSON.parse(localStorage.getItem('voucherDatabase'));
    return voucherDatabase.find((voucher) => voucher.name === voucherCode) ?? false;
}