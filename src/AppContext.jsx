import React from "react";

export const AppContext = React.createContext({
    productDatabase: [],
    basketbarClass: "",
    basketbarToggle: () => {},
    basketbarReset: () => {},
    basket: [],
    addToBasket: (product_code) => {},
    removeFromBasket: (product_code) => {},
    deleteFromBasket: (product_code) => {},
    totalAmount: 0,
    calculateTotalAmount: () => {},
    voucherRate: 0,
});
