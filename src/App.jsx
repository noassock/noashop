// #region IMPORT

import React from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom";

import { NavBar } from "./components/navigationbars/NavBar";
import { SideBar } from "./components/navigationbars/SideBar";
import { BasketBar } from "./components/navigationbars/BasketBar";

import { Home } from "./pages/Home";
import { Product } from "./pages/Product";
import { Basket } from "./pages/Basket";

import { findProduct } from "./lib/database";

import { AppContext } from "./AppContext";

// #endregion IMPORT

export class App extends React.Component {
    state = {
        productDatabase: [],
        basketbarClass: "d-none",
        basketbarToggle: () => {
            if (this.state.basketbarClass === "d-none") {
                this.setState({ basketbarClass: "d-block" });
            } else {
                this.setState({ basketbarClass: "d-none" });
            }
        },
        basketbarReset: () => {
            this.setState({ basketbarClass: "d-none" });
        },
        basket: [],
        addToBasket: (product_code) => {
            // Duplication du state contenant le panier.
            let basket = [...this.state.basket];

            // Recherche du produit dans le panier.
            let basketItem = basket.find(
                (basketItem) => basketItem.product.product_code === product_code
            );

            // Est-ce que le produit existe déjà dans le panier ?
            if (basketItem === undefined) {
                // Non, ajout initial du produit.
                basket.push({
                    product: findProduct(product_code),
                    quantity: 1,
                });
            } else {
                // Oui, mise à jour de la quantité du produit.
                basketItem.quantity++;
            }

            this.setState({refreshingPrices: true});

            // Mise à jour du panier.
            this.setState({ basket: basket });
            setTimeout(() => {
                this.state.calculateTotalAmount();
                this.setState({refreshingPrices: false});

            }, 1000);
        },
        removeFromBasket: (product_code) => {
            // Duplication du state contenant le panier.
            let basket = [...this.state.basket];

            // Recherche du produit dans le panier.
            let basketItem = basket.find(
                (basketItem) => basketItem.product.product_code === product_code
            );

            // Est-ce que le produit existe déjà dans le panier ?
            if (basketItem && basketItem.quantity > 1) {
                // Oui, mise à jour de la quantité du produit.
                basketItem.quantity--;
            }

            this.setState({refreshingPrices: true});

            // Mise à jour du panier.
            this.setState({ basket: basket });
            setTimeout(() => {
                this.state.calculateTotalAmount();
                this.setState({refreshingPrices: false});
            }, 1000);
        },
        deleteFromBasket: (product_code) => {
            // Duplication du state contenant le panier.
            let basket = [...this.state.basket];

            // Recherche du produit dans le panier.
            let basketItemIndex = basket.findIndex(
                (basketItem) => basketItem.product.product_code === product_code
            );

            // Est-ce que le produit existe déjà dans le panier ?
            if (basketItemIndex > -1) {
                basket.splice(basketItemIndex,1);
            }

            this.setState({refreshingPrices: true});

            // Mise à jour du panier.
            this.setState({ basket: basket });
            setTimeout(() => {
                this.state.calculateTotalAmount();
                this.setState({refreshingPrices: false});

            }, 1000);
        },
        totalAmount: 0,
        calculateTotalAmount: () => {
            let total = 0;
            if (this.state.basket.length) {
                for (const basketItem of this.state.basket) {
                    total += basketItem.quantity * basketItem.product.price;
                }
            }
            this.setState({ totalAmount: total });
        },
        voucherRate: 0,
        voucherDatabase: [],
        setVoucher: (newVoucherRate) => {
            this.setState({voucherRate: newVoucherRate});
        },
        refreshingPrices: false
    };

    componentDidMount() {
        if (localStorage.getItem("productDatabase") !== null) {
            this.setState({
                productDatabase: JSON.parse(
                    localStorage.getItem("productDatabase")
                ),
            });
        } else {
            const productsPromise = fetch("data.json");

            productsPromise
                .then((res) => res.json())
                .then((result) => {
                    this.setState({ productDatabase: result.products });
                    localStorage.setItem(
                        "productDatabase",
                        JSON.stringify(result.products)
                    );
                });
        }

        if (localStorage.getItem("voucherDatabase") !== null) {
            this.setState({
                voucherDatabase: JSON.parse(
                    localStorage.getItem("voucherDatabase")
                ),
            });
        } else {
            const vouchersPromise = fetch("data.json");

            vouchersPromise
                .then((res) => res.json())
                .then((result) => {
                    this.setState({ voucherDatabase: result.vouchers });
                    localStorage.setItem(
                        "voucherDatabase",
                        JSON.stringify(result.vouchers)
                    );
                });
        }
    }

    render() {
        return (
            <AppContext.Provider value={this.state}>
                <main className="row">
                    <BrowserRouter>
                        <header className="App-header">
                            <NavBar />
                            <BasketBar />
                        </header>

                        <Switch>
                            <Route exact path="/">
                                <SideBar
                                    database={this.state.productDatabase}
                                />
                                <Home database={this.state.productDatabase} />
                            </Route>
                            <Route exact path="/basket">
                                <Basket />
                            </Route>
                            <Route exact path="/product/:code">
                                <Product />
                            </Route>
                        </Switch>
                    </BrowserRouter>
                </main>
            </AppContext.Provider>
        );
    }
}
