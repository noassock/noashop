import React from "react";

import { AppContext } from "../AppContext";
import { ProductInBasket } from "../components/products/ProductInBasket";
import { findVoucher } from "../lib/database";

import { formatMoney, formatVoucher } from "../lib/utilities";

import "./basket.css";

export class Basket extends React.Component {
    static contextType = AppContext;

    state = {
        voucherText: "",
    };

    componentDidMount() {
        window.scrollTo(0, 0);
        this.context.basketbarReset();
    }

    voucherChangeInput = (event) => {
        let voucherInput = event.target.value.toUpperCase();
        this.setState({ voucherText: voucherInput });
    };

    checkVoucher = () => {
        const getVoucher = findVoucher(this.state.voucherText);
        if (getVoucher) {
            this.context.setVoucher(getVoucher.reduction);
        }
        this.setState({ voucherText: "" });
    };

    render() {
        return (
            <section className="container basket">
                {this.context.basket.length === 0 ? (
                    <article>
                        <a href="/">
                            <h2 className="alert alert-danger" role="alert">
                                Votre panier est bien vide 😥
                            </h2>
                        </a>
                    </article>
                ) : (
                    <section className="row">
                        <article className="col-12 col-sm-8">
                            <h2>Mon panier</h2>
                            <div className="products-in-basket">
                                <ProductInBasket />
                            </div>
                        </article>

                        <aside className="col-12 col-sm-4">
                            <h2>Récapitulatif</h2>
                            <div className="basket-voucher">
                                <p>Un code promo ?</p>
                                <div className="d-flex">
                                    <input
                                        value={this.state.voucherText}
                                        onChange={this.voucherChangeInput}
                                        className="form-control"
                                        placeholder="Ecrivez le ici"
                                    />
                                    <button
                                        className="btn btn-light"
                                        onClick={this.checkVoucher}
                                    >
                                        Valider
                                    </button>
                                </div>
                            </div>
                            <article className="basket-voucher">
                                {this.context.refreshingPrices ? (
                                    <div className="waiting">
                                        <div
                                            class="spinner-border"
                                            role="status"
                                        ></div>
                                    </div>
                                ) : (
                                    <div>
                                        <div className="d-flex justify-content-between">
                                            <p>Panier</p>
                                            <p>
                                                {formatMoney(
                                                    this.context.totalAmount
                                                )}
                                            </p>
                                        </div>
                                        <div className="d-flex justify-content-between">
                                            <p>Frais de livraison estimés</p>
                                            <p>Gratuit</p>
                                        </div>
                                        {this.context.voucherRate > 0 && (
                                            <div className="d-flex justify-content-between text-success">
                                                <p>
                                                    Promo appliquée ( -
                                                    {formatVoucher(
                                                        this.context.voucherRate
                                                    )}
                                                    )
                                                </p>
                                                <p>
                                                    -
                                                    {formatMoney(
                                                        this.context
                                                            .totalAmount *
                                                            this.context
                                                                .voucherRate
                                                    )}
                                                </p>
                                            </div>
                                        )}
                                        <div className="d-flex justify-content-between">
                                            <p>
                                                <strong>TOTAL</strong> (TVA
                                                Incluse)
                                            </p>
                                            <p>
                                                <strong>
                                                    {formatMoney(
                                                        this.context
                                                            .totalAmount *
                                                            (1 -
                                                                this.context
                                                                    .voucherRate)
                                                    )}
                                                </strong>
                                            </p>
                                        </div>
                                        <div className="d-flex">
                                            <button className="btn btn-primary flex-fill">
                                                Valider
                                            </button>
                                        </div>
                                    </div>
                                )}
                            </article>
                        </aside>
                    </section>
                )}
            </section>
        );
    }
}
