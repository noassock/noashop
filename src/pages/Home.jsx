//#region IMPORT

import React from "react";

import { ProductList } from "../components/products/ProductList";

import { AppContext } from "../AppContext";


//#endregion IMPORT

export class Home extends React.Component {

    static contextType = AppContext;

    componentDidMount(){
        window.scrollTo(0,0);
        this.context.basketbarReset();
    }

    render() {
        return (
            <section className="col-10">
                <h1 className="col-10">Accueil</h1>
                <ProductList database={this.props.database}/>
            </section>
        );
    };
};
