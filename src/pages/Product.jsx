import React from "react";

import { findProduct } from "../lib/database";
import { formatMoney } from "../lib/utilities";

import { AppContext } from "../AppContext";

export class Product extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);
        //Get the end of the url, and cut inside to get the params
        const productCodeUrl = window.location.pathname.substring(9);
        this.product = findProduct(productCodeUrl);
    }

    onClickAddToBasket = (event) => {
        this.context.addToBasket(event.target.dataset.product);
    };

    componentDidMount(){
        window.scrollTo(0,0);
        this.context.basketbarReset();
    }

    render() {
        if (this.product === false) {
            return (
                <main>
                    <a href="/">
                        <h2 className="alert alert-danger" role="alert">
                            Ce produit n'existe pas 🙃
                        </h2>
                    </a>
                </main>
            );
        }
        return (
            <main className="row">
                <img
                    src={`/images/products/${this.product.product_code.toLowerCase()}.jpg`}
                    className="col-6"
                    alt={this.product.title}
                />

                <div>
                    <h2>{this.product.title}</h2>
                    <h2>{this.product.platform}</h2>
                    <h2>{formatMoney(this.product.price)}</h2>
                    <button
                        className="btn btn-primary btn-sm"
                        data-product={this.product.product_code}
                        onClick={this.onClickAddToBasket}
                    >
                        Ajouter au panier
                    </button>
                </div>
            </main>
        );
    }
}
