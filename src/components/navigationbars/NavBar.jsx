//#region IMPORT

import React from "react";

// Composant de React Router
import { NavLink } from "react-router-dom";

import { AppContext } from "../../AppContext";

// Feuille de styles spécifique à cette page (importation via Webpack).
import "./navbar.css";

//#endregion IMPORT

export class NavBar extends React.Component {

    static contextType = AppContext;

    render() {
        return (
            <nav className="navbar">
                <NavLink exact className="nav-link title-noashop" to="/">
                    Noashop
                </NavLink>
                {/* <div>
                    <div className="form-inline">
                        <input
                            className="form-control"
                            placeholder="Rechercher un produit"
                            size="45"
                        ></input>
                        <button className="btn">🔍</button>
                    </div>
                </div> */}
                
                <button onClick={this.context.basketbarToggle} className="btn btn-primary">
                    Mon panier : <strong>{ this.context.basket.length }</strong> produits
                </button>
            </nav>
        );
    }
}
