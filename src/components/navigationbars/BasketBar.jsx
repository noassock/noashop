//#region IMPORT

import React from "react";

import { NavLink } from "react-router-dom";

import { AppContext } from "../../AppContext";

import { formatMoney } from "../../lib/utilities";
import { ProductInBar } from "../products/ProductInBar";

import "./basketbar.css";

//#endregion IMPORT

export class BasketBar extends React.Component {
    static contextType = AppContext;

    render() {
        return (
            <aside className="wrapper">
                <div
                    id="basketbar-bg"
                    className={this.context.basketbarClass}
                    onClick={this.context.basketbarToggle}
                ></div>

                <nav id="basketbar" className={this.context.basketbarClass}>
                    <div className="basketbar-part">
                        <ProductInBar/>
                    </div>
                    <div className="basketbar-part">
                        <p>Prix total (Hors livraison) : {formatMoney(this.context.totalAmount)}</p>
                    </div>
                    <NavLink exact className="nav-link" to="/basket">
                        <button className="btn btn-primary">
                            Voir mon panier
                        </button>
                    </NavLink>

                    <NavLink exact className="nav-link" to="/">
                        <button
                            className="btn btn-success"
                            onClick={this.context.basketbarToggle}
                        >
                            Continuer mes achats
                        </button>
                    </NavLink>
                </nav>
            </aside>
        );
    }
}
