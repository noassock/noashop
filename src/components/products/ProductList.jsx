//#region IMPORT

import React from "react";

import { Link } from "react-router-dom";

import { formatMoney } from "../../lib/utilities";

import { AppContext } from "../../AppContext";

import "./productlist.css";

//#endregion IMPORT

export class ProductList extends React.Component {
    static contextType = AppContext;

    onClickAddToBasket = (event) => {
        this.context.addToBasket(event.target.dataset.product);
    };

    render() {
        const productList = this.props.database.map((product, index) => {
            return (
                <article className="card" key={index}>
                    <Link
                        className="nav-link"
                        to={`/product/${product.product_code}`}
                    >
                        <img
                            src={`/images/products/${product.product_code.toLowerCase()}.jpg`}
                            className="card-img-top"
                            alt={product.title}
                        />
                    </Link>

                    <div className="card-body">
                        <Link
                            className="nav-link"
                            to={`/product/${product.product_code}`}
                        >
                            <h6 className="card-title">
                                {product.title} - {product.platform}
                            </h6>
                        </Link>

                        <h6 className="price" aria-label="Prix du produit">
                            {formatMoney(product.price)}
                        </h6>

                        <button
                            className="btn btn-primary btn-sm"
                            data-product={product.product_code}
                            onClick={this.onClickAddToBasket}
                        >
                            Ajouter au panier
                        </button>
                    </div>
                </article>
            );
        });
        return <section className="product-list col-10">{productList}</section>;
    }
}
