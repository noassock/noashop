//#region IMPORT

import React from "react";

import { Link } from "react-router-dom";

import { formatMoney } from "../../lib/utilities";

import { AppContext } from "../../AppContext";

import "./productinbar.css";

//#endregion IMPORT

export class ProductInBar extends React.Component {
    static contextType = AppContext;

    onClickAddToBasket = (event) => {
        this.context.addToBasket(event.target.dataset.product);
    };

    render() {
        const productList = this.context.basket.map(
            (productIteration, index) => {
                const { product } = productIteration;
                return (
                    <article className="card-custom d-flex" key={index}>
                        <Link
                            className=""
                            to={`/product/${product.product_code}`}
                        >
                            <img
                                src={`/images/products/${product.product_code.toLowerCase()}.jpg`}
                                className=""
                                alt={product.title}
                            />
                        </Link>

                        <div className="card-custom-body">
                            <div className="d-flex justify-content-between align-items-center">
                                <h6>{product.title}</h6>
                                <h6>x{productIteration.quantity}</h6>

                                <h6 aria-label="Prix du produit" className="price-in-bar">
                                    {formatMoney((product.price)*(productIteration.quantity))}
                                </h6>
                            </div>
                            {/* <div className="d-flex justify-content-end align-items-end">
                                <div>
                                    <button className="btn btn-sm custom-button">-</button>
                                        <h6>{productIteration.quantity}</h6>
                                    <button className="btn btn-sm">+</button>

                                </div>
                                </div> */}
                        </div>
                    </article>
                );
            }
        );

        return <section className="product-in-bar">{productList}</section>;
    }
}
