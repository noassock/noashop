//#region IMPORT

import React from "react";

import { Link } from "react-router-dom";

import { formatMoney } from "../../lib/utilities";

import { AppContext } from "../../AppContext";

import "./productinbasket.css";

//#endregion IMPORT

export class ProductInBasket extends React.Component {
    static contextType = AppContext;

    onClickAddToBasket = (event) => {
        this.context.addToBasket(event.target.dataset.product);
    };

    onClickRemoveFromBasket = (event) => {
        this.context.removeFromBasket(event.target.dataset.product);
    };

    onClickDeleteFromBasket = (event) => {
        this.context.deleteFromBasket(event.target.dataset.product);
    };

    render() {
        const productList = this.context.basket.map(
            (productIteration, index) => {
                const { product } = productIteration;
                return (
                    <article className="card-custom d-flex" key={index}>
                        <Link
                            className=""
                            to={`/product/${product.product_code}`}
                        >
                            <img
                                src={`/images/products/${product.product_code.toLowerCase()}.jpg`}
                                className=""
                                alt={product.title}
                            />
                        </Link>

                        <div className="card-custom-body">
                            <div className="d-flex justify-content-between align-items-start">
                                <h6>
                                    {product.title} - {product.platform}
                                </h6>

                                <h6 aria-label="Prix du produit">
                                    {formatMoney(
                                        product.price *
                                            productIteration.quantity
                                    )}
                                </h6>
                            </div>
                            <div className="d-flex justify-content-end align-items-end">
                                <div className="quantity-btn">
                                    <button
                                        className="btn btn-sm"
                                        data-product={product.product_code}
                                        onClick={this.onClickRemoveFromBasket}
                                    >
                                        -
                                    </button>
                                    <h6>{productIteration.quantity}</h6>
                                    <button
                                        className="btn btn-sm"
                                        data-product={product.product_code}
                                        onClick={this.onClickAddToBasket}
                                    >
                                        +
                                    </button>
                                </div>
                            </div>
                            <div className="d-flex justify-content-end delete-btn">
                                <button
                                    className="btn btn-sm btn-danger"
                                    data-product={product.product_code}
                                    onClick={this.onClickDeleteFromBasket}
                                >
                                    Supprimer
                                </button>
                            </div>
                        </div>
                    </article>
                );
            }
        );

        return <section className="product-in-basket">{productList}</section>;
    }
}
